<?php

echo "<!DOCTYPE html>";
echo "<html lang='en'>";
echo "<head>";
echo "<meta charset='utf-8'>";
echo "<title>Fedora Gold Block Explorer</title>";
echo "<meta name='viewport' content='width=device-width, initial-scale=1'>";

//echo "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' crossorigin='anonymous'>";
//echo "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css' integrity='sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp' crossorigin='anonymous'>";
//echo "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' integrity='sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa' crossorigin='anonymous'></script>";

echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/darkly/bootstrap.css' crossorigin='anonymous'>";

echo "<style>";

echo "ul.nav li a, ul.nav li a:visited { color: #f8c91c !important; }";
echo "a, h1, h2, h3 { color: #f8c91c !important; }";
echo "a:hover { background-color: #555; }";

echo "</style>";

echo "</head>";

echo "<body>";

echo "<nav class='navbar navbar-inverse navbar-fixed-top'>";
echo "   <div class='container-fluid'>";
echo "      <div class='navbar-header'>";
echo "         <a class='navbar-brand' href='http://explorer.fedoragold.com/'>Fedora Gold Block Explorer</a>";
echo "      </div>";
echo "      <ul class='nav navbar-nav'>";
echo "         <li><a href='#search'>Search Explorer</a></li>";
echo "      </ul>";
echo "      <ul class='nav navbar-nav navbar-right'>";
echo "         <li><a href='https://www.fedoragold.com/'>Project Home</a></li>";
echo "      </ul>";
echo "   </div>";
echo "</nav>";

echo "<div class='container-fluid'>";


//echo "<div class='jumbotron'>";

echo "<h3 style='padding-left:10px'>Network Summary</h3><br><br>";

//echo "</div>";


$cmd = "curl -X GET \"Accept: application/json\" -d '{\"jsonrpc\": \"2.0\"}' http://localhost:30159/getinfo";
$output = shell_exec($cmd);
$info =  json_decode($output, true);

$block = $info["height"]-1;
$cmd = "curl -X GET \"Accept: application/json\" -d '{\"height\":".$block.",\"jsonrpc\": \"2.0\"}' http://localhost:30159/getblock";
$output = shell_exec($cmd);
$block_info =  json_decode($output, true);

echo "<table class='table'>";
echo "<tr><td><h5>Last Block</h5></td><td>".  $info["height"] . "</td></tr>";
echo "<tr><td><h5>Peers</h5></td><td>".  $info["white_peerlist_size"] . "</td></tr>";
echo "<tr><td><h5>Connections</h5></td><td>".  $info["incoming_connections_count"] . "</td></tr>";
echo "<tr><td><h5>Difficulty</h5></td><td>".  number_format($info["difficulty"],0,"",",") . "</td></tr>";
echo "<tr><td><h5>Mined Coins</h5></td><td>".  number_format($block_info["alreadyGeneratedCoins"]/100000000,8,".",",") . "</td></tr>";
echo "<tr><td><h5>Last Reward</h5></td><td>".  number_format($block_info["reward"]/100000000,8,".",",") . "</td></tr>";
echo "<tr><td><h5>Last Hash</h5></td><td>".  $block_info["hash"] . "</td></tr>";
echo "</table>";

echo "<h3 style='padding-left:10px'>Latest Blocks</h3>";
echo "<table class='table table-striped'>";

// Heading
echo "<tr><th>Height</th><th>Hash</th><th>Difficulty</th><th>Timestamp</th><th>Reward</th></tr>";

// Remove the current block from the summary list
// echo "<tr><td><a href=getblock.php?height=".  $info["height"] . ">".  $info["height"] . "</a></td></tr>";

while($x < 10) {
    $x++;
    $block_number = $info["height"] - $x;

    echo "<tr>";

    echo "<td><a href=getblock.php?height=".  $block_number. ">".  $block_number . "</a></td>";


    // Retrieve the key details of each block for the summary page
    $cmd = "curl -X GET \"Accept: application/json\" -d '{\"height\":".$block_number.",\"jsonrpc\": \"2.0\"}' http://localhost:30159/getblock";
    $output = shell_exec($cmd);
    $block_data = json_decode($output, true);
    echo "<td>" . $block_data["hash"] . "</td>";
    echo "<td>".  number_format($block_data["difficulty"],0,"",",") . "</td>";
    echo "<td>". $block_data["timestamp"] . "</td>";
    echo "<td>". number_format($block_data["reward"]/100000000,8,".",",") . "</td>";

    echo "</tr>";
}
echo "</table>";
echo "<br>";
#echo "<pre>$output</pre>";

echo "<div class='jumbotron' id='search'>";

echo "<h3>Search the Network</h3><br><br>";

echo "<p>";
echo "<form action=getblock.php>";
echo "<div class='input-group row'>";
echo "<input type=text name=height placeholder='Get Block by Height' class='form-control'><button class='btn btn-outline-warning' type=submit>Retrieve</button>";
echo "</div>";
echo "</form>";
echo "</p>";

echo "<p>";
echo "<form action=getblockbyhash.php>";
echo "<div class='input-group row'>";
echo "<input type=text name=hash placeholder='Get Block by Hash' class='form-control'><button class='btn btn-outline-warning' type=submit>Retrieve</button>";
echo "</div>";
echo "</form>";
echo "</p>";

echo "<p>";
echo "<form action=gettransaction.php>";
echo "<div class='input-group row'>";
echo "<input type=text name=hash placeholder='Get Transaction by Hash' class='form-control'><button class='btn btn-outline-warning' type=submit>Retrieve</button>";
echo "</div>";
echo "</form>";
echo "</p>";

echo "</div>";

echo "</div>";

echo "<div class='text-center jumbotron'>";

echo "Copyright Fedora Gold Project 2019";

echo "</div>";


echo "<script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>";
echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' integrity='sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49' crossorigin='anonymous'></script>";
echo "<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>";


echo "</body>";

echo "</html>";
