<?php


$cmd = "curl -X GET \"Accept: application/json\" -d '{\"jsonrpc\": \"2.0\"}' http://localhost:30159/getinfo";
$output = shell_exec($cmd);
$info =  json_decode($output, true);

$block = $info["height"]-1;
$cmd = "curl -X GET \"Accept: application/json\" -d '{\"height\":".$block.",\"jsonrpc\": \"2.0\"}' http://localhost:30159/getblock";
$output = shell_exec($cmd);
$block_info =  json_decode($output, true);

// Mined coins: FED coins mined since launch on 02/SEP/2018
echo number_format($block_info["alreadyGeneratedCoins"]/100000000,8,".",",") ;