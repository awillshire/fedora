<?php


$cmd = "curl -X GET \"Accept: application/json\" -d '{\"jsonrpc\": \"2.0\"}' http://localhost:30159/getinfo";
$output = shell_exec($cmd);
$info =  json_decode($output, true);

$block = $info["height"]-1;
$cmd = "curl -X GET \"Accept: application/json\" -d '{\"height\":".$block.",\"jsonrpc\": \"2.0\"}' http://localhost:30159/getblock";
$output = shell_exec($cmd);
$block_info =  json_decode($output, true);

// Circulating supply: swapped + mined FED coins
// Altilly: 1,070,118,556 FED coin swapped
// Coinexchange: 580,348,969.2533412  + 23,182,528 FED coins swapped
// Project Team: y FED coins swapped
echo number_format(1070118556 + 580348969.2533412 + 23182528 + $block_info["alreadyGeneratedCoins"]/100000000,8,".",",") ;