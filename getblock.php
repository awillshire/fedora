<?php

echo "<!DOCTYPE html>";
echo "<html lang='en'>";
echo "<head>";
echo "<meta charset='utf-8'>";
echo "<title>Fedora Gold Block Explorer</title>";
echo "<meta name='viewport' content='width=device-width, initial-scale=1'>";

//echo "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' crossorigin='anonymous'>";
//echo "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css' integrity='sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp' crossorigin='anonymous'>";
//echo "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' integrity='sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa' crossorigin='anonymous'></script>";

echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/darkly/bootstrap.css' crossorigin='anonymous'>";

echo "<style>";

echo "ul.nav li a, ul.nav li a:visited { color: #f8c91c !important; }";
echo "a, h1, h2, h3 { color: #f8c91c !important; }";
echo "a:hover { color: #fff; background-color: #555; }";

echo "</style>";
echo "</head>";

echo "<body>";

echo "<nav class='navbar navbar-inverse navbar-fixed-top'>";
echo "   <div class='container-fluid'>";
echo "      <div class='navbar-header'>";
echo "         <a class='navbar-brand' href='http://explorer.fedoragold.com/'>Fedora Gold Block Explorer</a>";
echo "      </div>";
echo "      <ul class='nav navbar-nav navbar-right'>";
echo "         <li><a href='https://www.fedoragold.com/'>Project Home</a></li>";
echo "      </ul>";
echo "   </div>";
echo "</nav>";

echo "<div class='container-fluid'>";

$block = $string = preg_replace("/[^ \w]+/", "", $_GET["height"]);

$cmd = "curl -X GET \"Accept: application/json\" -d '{\"height\":".$block.",\"jsonrpc\": \"2.0\"}' http://localhost:30159/getblock";
$output = shell_exec($cmd);
$info =  json_decode($output, true);

echo "<h4><span style='color: #f8c91c; padding-right:20px; padding-left:10px'>Block Information for Block: </span><span style='padding-left:10px'> " . $block . "</span></h4><br>";

$nextblock = $block+1;
$lastblock = $block-1;

echo "<div style='margin-bottom:20px;'>";
echo "<a class='btn btn-outline-warning' role='button' style='color: #fff; margin-left:20px; padding-right:15px; padding-left:15px' href=getblock.php?height=" . (string)$lastblock . ">Last Block</a>";
if ($info["nonce"] > 0) {
	echo "<a class='btn btn-outline-warning' role='button' style='color: #fff; margin-left:20px; padding-right:15px; padding-left:15px' href=getblock.php?height=" . (string)$nextblock . ">Next Block</a>"; 
}
echo "</div>";
echo "<br>";


echo "<table class='table'>";
echo "<tr><td>Block Number:</td><td>" . $block . "</td></tr>";
echo "<tr><td>Mined Coins:</td><td>" . number_format($info["alreadyGeneratedCoins"]/100000000,2,".",",") . "</td></tr>";
echo "<tr><td>Block Size:</td><td>" . $info["blocksize"] . "</td></tr>";
echo "<tr><td>Current Median Size:</td><td>" . $info["currentmediansize"] . "</td></tr>";
echo "<tr><td>Depth:</td><td>" . $info["depth"] . "</td></tr>";
echo "<tr><td>Difficulty:</td><td>".  number_format($info["difficulty"],0,"",",") . "</td></tr>";
echo "<tr><td>Hash:</td><td>" . $info["hash"] . "</td></tr>";
echo "<tr><td>Major Version:</td><td>" . $info["major_version"] . "</td></tr>";
echo "<tr><td>Minor Version:</td><td>" . $info["minor_version"] . "</td></tr>";
echo "<tr><td>Nonce:</td><td>" . $info["nonce"] . "</td></tr>";
echo "<tr><td>Reward</td><td>".  number_format($info["reward"]/100000000,2,".",",") . "</td></tr>";
echo "<tr><td>Status:</td><td>" . $info["status"] . "</td></tr>";
echo "<tr><td>Timestamp:</td><td>" . $info["timestamp"] . "</td></tr>";
echo "<tr><td>Date ('m/d/Y h:m:s'):</td><td>" . date('m/d/Y h:m:s',$info["timestamp"]) . "</td></tr>";
echo "<tr><td>Previous Hash:</td><td>" . $info["prev_hash"] . "</td></tr>";
echo "<tr><td>Transaction Hashes:</td><td>" ;
foreach ($info["transactionHashes"] as $value) {
    echo "<a href=gettransaction.php?hash=$value>".$value."</a><br>" ;
}
echo "</td></tr>";
echo "</table>";

echo "</div>";

echo "<div class='text-center jumbotron'>";

echo "Copyright Fedora Gold Project 2019";

echo "</div>";


echo "<script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>";
echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' integrity='sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49' crossorigin='anonymous'></script>";
echo "<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>";


echo "</body>";

echo "</html>";
