<?php

echo "<!DOCTYPE html>";
echo "<html lang='en'>";
echo "<head>";
echo "<meta charset='utf-8'>";
echo "<title>Fedora Gold Block Explorer</title>";
echo "<meta name='viewport' content='width=device-width, initial-scale=1'>";

//echo "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' crossorigin='anonymous'>";
//echo "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css' integrity='sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp' crossorigin='anonymous'>";
//echo "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' integrity='sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa' crossorigin='anonymous'></script>";

echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/darkly/bootstrap.css' crossorigin='anonymous'>";

echo "<style>";

echo "ul.nav li a, ul.nav li a:visited { color: #f8c91c !important; }";
echo "a, h1, h2, h3 { color: #f8c91c !important; }";
echo "a:hover { color: #fff; background-color: #555; }";

echo "</style>";
echo "</head>";

echo "<body>";

echo "<nav class='navbar navbar-inverse navbar-fixed-top'>";
echo "   <div class='container-fluid'>";
echo "      <div class='navbar-header'>";
echo "         <a class='navbar-brand' href='http://explorer.fedoragold.com/'>Fedora Gold Block Explorer</a>";
echo "      </div>";
echo "      <ul class='nav navbar-nav navbar-right'>";
echo "         <li><a href='https://www.fedoragold.com/'>Project Home</a></li>";
echo "      </ul>";
echo "   </div>";
echo "</nav>";

echo "<div class='container-fluid'>";

$hash = $string = preg_replace("/[^ \w]+/", "", $_GET["hash"]);

echo "<h4><span style='color: #f8c91c; padding-right:20px; padding-left:10px'>Get Transaction: </span><span style='padding-left:10px'> " . $hash . "</span></h4><br>";

$cmd = "curl -X GET \"Accept: application/json\" -d '{\"txhash\":\"".$hash."\",\"jsonrpc\": \"2.0\"}' http://localhost:30159/gettransaction";
$output = shell_exec($cmd);
$info =  json_decode($output, true);

echo "<table class='table'>";
echo "<tr><td>Transaction:</td><td>" . $hash . "</td></tr>";
echo "<tr><td>Fee:</td><td>" . $info["fee"] . "</td></tr>";
echo "<tr><td>Block Number:</td><td><a href=getblock.php?height=" . $info["blockheight"] . ">" .$info["blockheight"] . "</a></td></tr>";
echo "<tr><td>Block:</td><td>" . $info["block"] . "</td></tr>";
echo "<tr><td>Extra:</td><td>" . $info["extra"] . "</td></tr>";
echo "<tr><td>Hash:</td><td>" . $info["hash"] . "</td></tr>";
echo "<tr><td>global_output_indexes:</td><td>" ;
foreach ($info["global_output_indexes"] as $value) {
    echo $value ."<br>" ;
}
echo "</td></tr>";
echo "<tr><td>Input Amount:</td><td>" . $info["inputamt"] . "</td></tr>";
echo "<tr><td>Output Amount:</td><td>" . $info["outputamt"] . "</td></tr>";
echo "<tr><td>Output Keys:</td><td>";
foreach ($info["outputskeys"] as $value) {
    echo $value ."<br>" ;
}
echo "</td></tr>";
echo "<tr><td>Output Amounts:</td><td>";
foreach ($info["outputsamts"] as $value) {
    echo $value ."<br>" ;
}
echo "</td></tr>";
echo "<tr><td>Orphan Status:</td><td>" . $info["orphan_status"] . "</td></tr>";
echo "<tr><td>Payment ID:</td><td>" . $info["paymentid"] . "</td></tr>";
echo "<tr><td>Prefix:</td><td>" . $info["prefix"] . "</td></tr>";
echo "<tr><td>Status:</td><td>" . $info["status"] . "</td></tr>";
echo "<tr><td>txsize:</td><td>" . $info["txsize"] . "</td></tr>";
echo "<tr><td>Unlock Time:</td><td>" . $info["unlocktime"] . "</td></tr>";
echo "</table>";

#echo "<pre>$output</pre>";

echo "<br>";
echo "</div>";

echo "<div class='text-center jumbotron'>";

echo "Copyright Fedora Gold Project 2019";

echo "</div>";


echo "<script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>";
echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' integrity='sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49' crossorigin='anonymous'></script>";
echo "<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>";


echo "</body>";

echo "</html>";
